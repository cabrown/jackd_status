install:
	pip install .
	cp external/jackd_status.desktop ${HOME}/.config/autostart
	mkdir -p ${HOME}/.local/share/applications # doesn't seem to be present on a clean manjaro install
	cp external/jackd_status.desktop ${HOME}/.local/share/applications
ifneq ($(wildcard ${HOME}/.jackd_cmd.txt),"")
	cp external/jackd_cmd.txt ${HOME}/.jackd_cmd.txt
else
	cp -f external/jackd_cmd.txt ${HOME}/.jackd_cmd.txt.new
endif

uninstall:
	-rm $HOME/.config/autostart/jackd_status.desktop
	-rm $HOME/.local/share/applications/jackd_status.desktop
	python -m pip uninstall -y jackd_status
